<?php

namespace App\Http\Controllers;

use App\Exports\EmployeeExport;
use App\Models\Employee;
use App\Models\Posisi;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;
use PDF;
use PhpOffice\PhpSpreadsheet\Writer\Pdf as WriterPdf;

class Employeecontroller extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // mengambil data
        $data = Employee::paginate(4);
        // $data = Posisi::paginate(4);
        return view('app.indexview',['emp'=>$data]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //Input member
        $atasan = DB::select("SELECT * from employee");
        $company = DB::select("SELECT * from company");
        return view('app.createview', compact('atasan','company'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //

        $this->validate($request,[
            'nama' => 'required|min:5',
            'atasan_id' => 'required',
            'posisi_id' => 'required',
            'company_id' => 'required',
        ]);

        DB::table('employee')->insert([
            'nama' => $request->nama,
            'atasan_id' => $request->atasan_id,
            'company_id' => $request->company_id,
        ]);

        return redirect('/employee/tambah');
    }

    // public function validate(Request $request){
    //     $this->validate($request,[
    //         'nama' => 'required|min:5',
    //         'atasan_id' => 'required',
    //         'company_id' => 'required',
    //     ]);

    //     // redirect('/employee/store');
    // }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $employee = DB::table('employee')->where('id',$id)->first();
        $atasan = Employee::all();
        $company = DB::select("SELECT * from company");
        // echo $employee;
        return view('app.editview',compact('employee','atasan','company'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //memasukkan data ke database
        DB::table('employee')->where('id',$id)->update([
            'nama' =>$request->nama,
            'atasan_id' => $request->atasan_id,
            'company_id' => $request->company_id,
        ]);

        return redirect('/');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //menghapus data ke database
        DB::table('employee')->where('id',$id)->delete();

        return back();
    }

    public function export_excel(){
        return Excel::download(new EmployeeExport, 'Employee.xlsx');
    }

    public function export_pdf(){
        $emp = Employee::all();
        $pdf = PDF::loadview('pdfprint',['emp'=>$emp]);

        return $pdf->download('Employee.pdf');
    }
}
