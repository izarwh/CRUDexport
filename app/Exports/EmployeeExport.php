<?php

namespace App\Exports;

use App\Models\Employee;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;

class EmployeeExport implements FromCollection,WithHeadings,WithMapping
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        return Employee::with('company')
        ->with('posisi')
        ->get();
    }

    public function map($row): array
    {
        return [
            $row->id,
            $row->nama,
            $row->posisi->posisi,
            $row->company->nama
        ];
    }

    public function headings(): array
    {
        return [
            'id',
            'nama',
            'Posisi',
            'company',
        ];
    }
}
