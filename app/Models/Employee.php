<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Employee extends Model
{
    use HasFactory;

    protected $table = 'employee';

    protected $fillable = ['nama','posisi_id','atasan_id','company_id'];

    protected $primaryKey = 'id';

    public $timestamps = false;

    public function company(){
        return $this->belongsTo('App\Models\Company','company_id');
    }

    public function posisi(){
        return $this->belongsTo('App\Models\Posisi','posisi_id');
    }
}
