<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Posisi extends Model
{
    use HasFactory;

    protected $table = 'posisi';

    protected $fillable = ['posisi'];

    protected $primaryKey = 'posisi_id';

    public $timestamps = false;

    public function employee(){
        return $this->hasMany('App\Models\Employee');
    }
}
