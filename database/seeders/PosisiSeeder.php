<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class PosisiSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('posisi')->insert([
            'posisi' => 'CEO'
        ]);
        DB::table('posisi')->insert([
            'posisi' => 'Direktur'
        ]);
        DB::table('posisi')->insert([
            'posisi' => 'Manager'
        ]);
        DB::table('posisi')->insert([
            'posisi' => 'Staff'
        ]);
    }
}
