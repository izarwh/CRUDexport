<?php

namespace Database\Seeders;

use Illuminate\Support\Facades\DB;
use Illuminate\Database\Seeder;

class EmployeeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //memasukan data ke table EmployeeSeeder
        DB::table('employee')->insert([
            'nama' => 'Pak Budi',
            'posisi_id' => 1,
            'atasan_id' => null,
            'company_id' => 1
        ]);
        DB::table('employee')->insert([
            'nama' => 'Pak Tono',
            'posisi_id' => 2,
            'atasan_id' => 1,
            'company_id' => 1
        ]);
        DB::table('employee')->insert([
            'nama' => 'Pak Totok',
            'posisi_id' => 2,
            'atasan_id' => 1,
            'company_id' => 1
        ]);
        DB::table('employee')->insert([
            'nama' => 'Bu Sinta',
            'posisi_id' => 3,
            'atasan_id' => 2,
            'company_id' => 1
        ]);
        DB::table('employee')->insert([
            'nama' => 'Bu Novi',
            'posisi_id' => 3,
            'atasan_id' => 3,
            'company_id' => 1
        ]);
        DB::table('employee')->insert([
            'nama' => 'Andre',
            'posisi_id' => 4,
            'atasan_id' => 4,
            'company_id' => 1
        ]);
        DB::table('employee')->insert([
            'nama' => 'Dono',
            'posisi_id' => 4,
            'atasan_id' => 4,
            'company_id' => 1
        ]);
        DB::table('employee')->insert([
            'nama' => 'Ismir',
            'posisi_id' => 4,
            'atasan_id' => 5,
            'company_id' => 1
        ]);
        DB::table('employee')->insert([
            'nama' => 'Anto',
            'posisi_id' => 4,
            'atasan_id' => 5,
            'company_id' => 1
        ]);
    }
}
