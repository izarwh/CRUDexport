<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Employeecontroller;
use App\Http\Controllers\Companycontroller;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/index', function () {
    return view('welcome');
});

Route::get('/',[Employeecontroller::class,'index']) -> name('index');
Route::get('/employee/tambah',[Employeecontroller::class,'create']) ->name('tambah');
Route::post('/employee/store',[Employeecontroller::class,'store']) ->name('store');
Route::get('/employee/edit/{id}',[Employeecontroller::class,'edit']) ->name('edit');
Route::post('/employee/update/{id}',[Employeecontroller::class,'update']) ->name('update');
Route::get('/employee/delete/{id}',[Employeecontroller::class,'destroy']) ->name('delete');
Route::post('/employee/validate',[Employeecontroller::class,'validate'])->name('validate');
Route::get('/employee/excel',[Employeecontroller::class,'export_excel']) ->name('excel');
Route::get('/employee/pdf',[Employeecontroller::class,'export_pdf']) ->name('pdf');



Route::get('/company/index',[Companycontroller::class,'index']) ->name('indexcompany');
Route::get('/company/edit/{id}',[Companycontroller::class,'edit']) -> name('editcompany');
Route::post('/company/update/{id}',[Companycontroller::class,'update']) -> name('updatecompany');
Route::get('/company/delete/{id}',[Companycontroller::class,'destroy']) -> name('deletecompany');
Route::get('/company/tambah',[Companycontroller::class,'create']) -> name('tambahcompany');
Route::post('/company/store',[Companycontroller::class,'store']) -> name('storecompany');