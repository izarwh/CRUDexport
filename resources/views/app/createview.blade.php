@extends('layouts.nav')
@section('title','input Data')
@section('content')
<div class="container">
    @if (count($errors) > 0)
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
    @endif()
    <form action={{ route('store') }} method="post">
        @csrf
        <div class="form-group">
            <label for="Nama">Nama</label>
            <input class="form-control" id="nama" name="nama" placeholder="Nama">
        </div>
        <div></div>
        <div class="form-group">
            <label for="IDAtasan">Atasan</label>
            <select class="form-control" id="atasan_id" name="atasan_id">
                @foreach($atasan as $atasan)
                <option value='{{ $atasan->id }}'>{{ $atasan -> nama }}</option>
                @endforeach
                {{-- <option value>1</option> --}}
            </select>
            
        </div>
        <div class="form-group">
            <label for="IDCOmpany">Company</label>
            <select class="form-control" id="company_id" name="company_id">
                @foreach($company as $comp)
                    <option value="{{ $comp->id}}">{{ $comp ->nama }}</option>
                @endforeach
                    {{-- <option>1</option> --}}
            </select>
        </div>
            <button type="submit" class="btn btn-primary">Submit</button>
    </form>
</div>
@endsection