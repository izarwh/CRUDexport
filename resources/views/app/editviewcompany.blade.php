@extends('layouts.nav')
@section('title','Edit Company')
@section('content')
<div class="container">
    <form action={{ route('updatecompany',['id' => $comp->id]) }} method="post">
        @csrf
        <div class="form-group">
            <label for="Nama">Nama</label>
            <input class="form-control" id="nama" name="nama" placeholder="Nama Company" value="{{ $comp -> nama }}">
        </div>
        <div></div>
        <div class="form-group">
            <label for="alamat">Alamat</label>
            <input class="form-control" id="alamat" name="alamat" placeholder="Alamat Company" value="{{ $comp -> alamat }}">
        </div>
            <button type="submit" class="btn btn-primary">Submit</button>
    </form>
</div>
@endsection