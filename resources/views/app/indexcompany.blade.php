@extends('layouts.nav')
@section('title','Menampilkan Company')
@section('content')

<table class="table">
    <thead>
        <tr>
          <th scope="col">id</th>
          <th scope="col">Nama</th>
          <th scope="col">alamat</th>
        </tr>
      </thead>
      <tbody>
        @foreach($comp as $comp)
        <tr>
            <th scope="row">{{ $comp-> id }}</th>
            <td>{{ $comp -> nama}}</td>
            <td>{{ $comp -> alamat }}</td>
            {{-- id harus sama denganyang ada di route --}}
            <td><a href="{{route('editcompany',['id' => $comp -> id])}}" class="btn btn-light">edit</a><a href="{{route('deletecompany',['id' => $comp -> id])}}" class="btn btn-danger">hapus</a></td>
        </tr>
        @endforeach
      </tbody>
</table>
@endsection