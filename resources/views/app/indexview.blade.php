@extends('layouts.nav')
@section('title','Menampilkan semua employee')
@section('content')

<div class="container">
  <div class="row">
    <div class="row">
      <a class="btn btn-success" href="{{ route('excel') }}">Download Excel</a>
      <a class="btn btn-light" href="{{ route('pdf') }}">Download PDF</a>
    </div>
    
    <table class="table table-striped">
      <thead>
        <tr>
          <th scope="col">id</th>
          <th scope="col">Nama</th>
          <th scope="col">id atasan</th>
          <th scope="col">Company</th>
          <th scope="col">Posisi</th>
        </tr>
      </thead>
      <tbody>
        @foreach($emp as $data)
        <tr>
          <th scope="row">{{ $data-> id }}</th>
          <td>{{ $data->nama}}</td>
          <td>{{ $data->atasan_id }}</td>
          <td>{{ $data->company->nama }}</td>
          <td>{{ $data->posisi->posisi }}</td>
          {{-- id harus sama denganyang ada di route --}}
          <td><a href="{{route('edit',['id' => $data -> id])}}" class="btn btn-light">edit</a><a href="{{route('delete',['id' => $data -> id])}}" class="btn btn-danger">hapus</a></td>
        </tr>
        @endforeach
      </tbody>
    </table>
  </div>
  <div class="row">
    <div class="d-flex justify-content-center">
      {{ $emp ->links('pagination::bootstrap-4') }}
    </div>
  </div>
</div>
@endsection