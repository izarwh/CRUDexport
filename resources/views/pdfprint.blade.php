<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>
    
    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <title>Employee PDF</title>
</head>
<body>
    <div class="container">
        <div class="row">
          <a class="btn btn-success" href="{{ route('excel') }}">Download Excel</a>
          <table class="table table-bordered">
            <thead>
              <tr>
                <th scope="col">id</th>
                <th scope="col">Nama</th>
                <th scope="col">id atasan</th>
                <th scope="col">Company</th>
                <th scope="col">Posisi</th>
              </tr>
            </thead>
            <tbody>
              @foreach($emp as $data)
                <tr>
                    <th scope="row">{{ $data-> id }}</th>
                    <td>{{ $data->nama}}</td>
                    <td>{{ $data->atasan_id }}</td>
                    <td>{{ $data->company->nama }}</td>
                    <td>{{ $data->posisi->posisi }}</td>
                </tr>
              @endforeach
            </tbody>
          </table>
        </div>
    </div>
</body>
</html>